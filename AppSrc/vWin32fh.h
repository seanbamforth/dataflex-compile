//TH-Header
//*****************************************************************************************
// Copyright (c)  2004 KURANT Project
// All rights reserved.
//
// $FileName    : G:\Projects\vWin32fh\vWin32fh.h
// $ProjectName : Windows 32 bits file handling
// $Authors     : Wil van Antwerpen
// $Created     : 19.02.2004  19:25
// $Type        : BSD (as in do with it whatever you like)
//
// Contents:
//  This file contains the Windows API external function call definitions and
//  constants as they are used in the vWin32fh package.
//*****************************************************************************************
//TH-RevisionStart
//TH-RevisionEnd

Define CS_DDE_ERR_FILE_NOT_FOUND  For "The specified file was not found.\n"
Define CS_DDE_ERR_PATH_NOT_FOUND  For "The specified path was not found.\n"
Define CS_DDE_ERR_BAD_FORMAT      For "The .EXE file is invalid.\n"
Define CS_DDE_ERR_ACCESSDENIED    For "The operating system denied access to the specified file.\n"
Define CS_DDE_ERR_ASSOCINCOMPLETE For "The filename association is incomplete or invalid.\n"
Define CS_DDE_ERR_DDEBUSY         For "The DDE transaction could not be completed because other DDE\ntransactions were being processed.\n"
Define CS_DDE_ERR_DDEFAIL         For "The DDE transaction failed.\n"
Define CS_DDE_ERR_DDETIMEOUT      For "The DDE transaction could not be completed,\nbecause the request timed out.\n"
Define CS_DDE_ERR_DLLNOTFOUND     For "The specified dynamic-link library was not found.\n"
Define CS_DDE_ERR_NOASSOC         For "There is no application associated with the given filename extension.\n"
Define CS_DDE_ERR_OOM             For "There is not enough free memory available to complete the operation.\n"
Define CS_DDE_ERR_PNF             For "The specified path was not found.\n"
Define CS_DDE_ERR_SHARE           For "A sharing violation occurred.\n"
Define CS_DDE_ERR_UNKNOWN_LINE1   For "Unknown DDE-error occurred.\nErrornumber"
Define CS_DDE_ERR_UNKNOWN_LINE2   For ".\n"
Define CS_DDE_ERR_HANDL_PAKTC     For "\nPress a key to continue...\n\n"
Define CS_DDE_ERR_HANDL_CAPTION   For "a DDE-error occured"


Define vMax_Path     For |CI260
Define vMinChar      For |CI$80
Define vMaxChar      For |CI$7F
Define vMinShort     For |CI$8000
Define vMaxShort     For |CI$7FFF
Define vMinLong      For |CI$80000000
Define vMaxLong      For |CI$7FFFFFFF
Define vMaxByte      For |CI$FF
Define vMaxWord      For |CI$FFFF
Define vMaxDword     For |CI$FFFFFFFF



// For FindFirstFile
Define vINVALID_HANDLE_VALUE   For |CI-1
Define vINVALID_FILE_SIZE      For |CI$FFFFFFFF
Define vERROR_NO_MORE_FILES    For |CI18

// The defines below can be used to find out what kind of error has occured if
// the API-call ShellExecute is used.
Define vERROR_FILE_NOT_FOUND   For |CI0002
Define vERROR_PATH_NOT_FOUND   For |CI0003
Define vERROR_BAD_FORMAT       For |CI0011
Define vSE_ERR_ACCESSDENIED    For |CI0005
Define vSE_ERR_ASSOCINCOMPLETE For |CI0027
Define vSE_ERR_DDEBUSY         For |CI0030
Define vSE_ERR_DDEFAIL         For |CI0029
Define vSE_ERR_DDETIMEOUT      For |CI0028
Define vSE_ERR_DLLNOTFOUND     For |CI0032
Define vSE_ERR_FNF             For |CI0002
Define vSE_ERR_NOASSOC         For |CI0031
Define vSE_ERR_OOM             For |CI0008
Define vSE_ERR_PNF             For |CI0003
Define vSE_ERR_SHARE           For |CI0026


// C-Structure
//typedef struct _browseinfo {
//    HWND hwndOwner;
//    LPCITEMIDLIST pidlRoot;
//    LPSTR pszDisplayName;
//    LPCSTR lpszTitle;
//    UINT ulFlags;
//    BFFCALLBACK lpfn;
//    LPARAM lParam;
//    int iImage;
//} BROWSEINFO, *PBROWSEINFO, *LPBROWSEINFO;

//declare C structure struct_browseinfo
//as documented in MSDN under Windows Shell API
Type vtBrowseInfo
  Field vtBrowseInfo.hWndOwner      as Handle
  Field vtBrowseInfo.pIDLRoot       as Pointer
  Field vtBrowseInfo.pszDisplayName as Pointer
  Field vtBrowseInfo.lpszTitle      as Pointer
  Field vtBrowseInfo.ulFlags        as dWord
  Field vtBrowseInfo.lpfnCallback   as Pointer
  Field vtBrowseInfo.lParam         as dWord
  Field vtBrowseInfo.iImage         as dWord
End_Type // tBrowseInfo

// Browsing for directory.
Define vBIF_RETURNONLYFSDIRS   For |CI$0001  // For finding a folder to start document searching
Define vBIF_DONTGOBELOWDOMAIN  For |CI$0002  // For starting the Find Computer
Define vBIF_STATUSTEXT         For |CI$0004  // Includes a status area in the dialog box.
                                            // The callback function can set the status text by
                                            // sending messages to the dialog box.
Define vBIF_RETURNFSANCESTORS  For |CI$0008  // Only returns file system ancestors.

Define vBIF_BROWSEFORCOMPUTER  For |CI$1000  // Browsing for Computers.
Define vBIF_BROWSEFORPRINTER   For |CI$2000  // Browsing for Printers

// message from browser
//Define BFFM_INITIALIZED        1
//Define BFFM_SELCHANGED         2

// messages to browser
//Define BFFM_SETSTATUSTEXT      (WM_USER + 100)
//Define BFFM_ENABLEOK           (WM_USER + 101)
//Define BFFM_SETSELECTION       (WM_USER + 102)


External_function vWin32_SHBrowseForFolder "SHBrowseForFolder" shell32.dll ;
  Pointer lpsBrowseInfo Returns dWord

External_function vWin32_SHGetPathFromIDList "SHGetPathFromIDList" shell32.dll ;
  Pointer pidList Pointer lpBuffer Returns dWord

External_function vWin32_CoTaskMemFree "CoTaskMemFree" ole32.dll Pointer pV Returns Integer




Type vtSecurity_attributes
  Field vtSecurity_attributes.nLength        as dWord
  Field vtSecurity_attributes.lpDescriptor   as Pointer
  Field vtSecurity_attributes.bInheritHandle as Integer
End_Type // vtSecurity_attributes

//nLength:
// Specifies the size, in bytes, of this structure. Set this value to the size of the
// SECURITY_ATTRIBUTES structure.
// Windows NT: Some functions that use the SECURITY_ATTRIBUTES structure do not verify the
// value of the nLength member. However, an application should still set it properly.
// That ensures current, future, and cross-platform compatibility.
//
//lpSecurityDescriptor:
// Points to a security descriptor for the object that controls the sharing of it.
// If NULL is specified for this member, the object may be assigned the default security
// descriptor of the calling process.
//
//bInheritHandle:
// Specifies whether the returned handle is inherited when a new process is created.
// If this member is TRUE, the new process inherits the handle.


// BOOL CreateDirectory(
//    LPCTSTR lpPathName,
//    LPSECURITY_ATTRIBUTES lpSecurityAttributes  // pointer to a security descriptor
//   );
//
// lpPathName
//  Points to a null-terminated string that specifies the path of the directory
//  to be created.
//  There is a default string size limit for paths of MAX_PATH characters.
//  This limit is related to how the CreateDirectory function parses paths.
// lpSecurityAttributes
//  Pointer to a SECURITY_ATTRIBUTES structure als called a security descriptor that
//  determines whether the returned handle can be inherited by child processes.
//  If lpSecurityAttributes is NULL, the handle cannot be inherited.
// Returns:
//  If the function succeeds, the return value is nonzero.
//  If the function fails, the return value is zero. To get extended error information, call GetLastError.
External_function vWin32_CreateDirectory "CreateDirectoryA" kernel32.dll ;
  Pointer lpPathName Pointer lpSecurity_Attributes Returns Integer


// lpPathName
//  Points to a null-terminated string that specifies the path of the directory
//  to be removed.
//  There is a default string size limit for paths of MAX_PATH characters.
// Returns:
//  If the function succeeds, the return value is nonzero.
//  If the function fails, the return value is zero. To get extended error information, call GetLastError.
External_function vWin32_RemoveDirectory "RemoveDirectoryA" kernel32.dll ;
  Pointer lpPathName Returns Integer



// The ShellExecute function opens or prints a specified file. The file can be an
// executable file or a document file.
//
// Operation can be one of the following:
//    "OPEN"  The function opens the file specified by lpFile.
//            The file can be an executable file or a document file.
//            The file can be a folder to open.
//    "PRINT" The function prints the file specified by lpFile.
//            The file should be a document file. If the file is an executable file,
//            the function opens the file, as if �open� had been specified.
//  "EXPLORE" The function explores the folder specified by lpFile.
//
// Return Values:
//
// If the function succeeds, the return value is the instance handle of the application that
// was run, or the handle of a dynamic data exchange (DDE) server application.
// If the function fails, the return value is an error value that is less than or equal to 32.
//
// The following table lists these error values:
// Public Const ERROR_FILE_NOT_FOUND = 2&
// Public Const ERROR_PATH_NOT_FOUND = 3&
// Public Const ERROR_BAD_FORMAT = 11&
// Public Const SE_ERR_ACCESSDENIED = 5
// Public Const SE_ERR_ASSOCINCOMPLETE = 27
// Public Const SE_ERR_DDEBUSY = 30
// Public Const SE_ERR_DDEFAIL = 29
// Public Const SE_ERR_DDETIMEOUT = 28
// Public Const SE_ERR_DLLNOTFOUND = 32
// Public Const SE_ERR_FNF = 2
// Public Const SE_ERR_NOASSOC = 31
// Public Const SE_ERR_OOM = 8
// Public Const SE_ERR_PNF = 3
// Public Const SE_ERR_SHARE = 26



// Code to open the program that is associated with the selected file.
//
// External function call used in Procedure DoStartDocument
External_function vWin32_ShellExecute "ShellExecuteA" shell32.dll ;
  Handle hWnd ;
  Pointer lpOperation ;
  Pointer lpFile ;
  Pointer lpParameters ;
  Pointer lpDirectory ;
  Dword iShowCmd Returns Handle



#IFDEF vFO_MOVE
#ELSE

#Replace vFO_MOVE           |CI$0001
#Replace vFO_COPY           |CI$0002
#Replace vFO_DELETE         |CI$0003
#Replace vFO_RENAME         |CI$0004

#Replace vFOF_MULTIDESTFILES     |CI$0001
#Replace vFOF_CONFIRMMOUSE       |CI$0002
#Replace vFOF_SILENT             |CI$0004  // don't create progress/report
#Replace vFOF_RENAMEONCOLLISION  |CI$0008
#Replace vFOF_NOCONFIRMATION     |CI$0010  // Don't prompt the user.
#Replace vFOF_WANTMAPPINGHANDLE  |CI$0020  // Fill in SHFILEOPSTRUCT.hNameMappings
                                          // Must be freed using SHFreeNameMappings
#Replace vFOF_ALLOWUNDO          |CI$0040
#Replace vFOF_FILESONLY          |CI$0080  // on *.*, do only files
#Replace vFOF_SIMPLEPROGRESS     |CI$0100  // means don't show names of files
#Replace vFOF_NOCONFIRMMKDIR     |CI$0200  // don't confirm making any needed dirs

Type vtShFileOpStruct
  Field vtShFileOpStruct.hWnd                   as Handle
  Field vtShFileOpStruct.wFunc                  as Integer
  Field vtShFileOpStruct.pFrom                  as Pointer
  Field vtShFileOpStruct.pTo                    as Pointer
  Field vtShFileOpStruct.fFlags                 as Short
  Field vtShFileOpStruct.fAnyOperationsAborted  as Short
  Field vtShFileOpStruct.hNameMappings          as Pointer
  Field vtShFileOpStruct.lpszProgressTitle      as Pointer // only used if FOF_SIMPLEPROGRESS
End_Type // tShFileOpStruct

// hwnd
//   Handle of the dialog box to use to display information about the status of the operation.

// wFunc
//   Operation to perform. This member can be one of the following values:
//     FO_COPY     Copies the files specified by pFrom to the location specified by pTo.
//     FO_DELETE   Deletes the files specified by pFrom (pTo is ignored).
//     FO_MOVE     Moves the files specified by pFrom to the location specified by pTo.
//     FO_RENAME   Renames the files specified by pFrom.

// pFrom
//   Pointer to a buffer that specifies one or more source file names. Multiple names must
//   be null-separated. The list of names must be double null-terminated.

// pTo
//   Pointer to a buffer that contains the name of the destination file or directory. The
//   buffer can contain mutiple destination file names if the fFlags member specifies
//   FOF_MULTIDESTFILES. Multiple names must be null-separated. The list of names must be
//   double null-terminated.

// fAnyOperationsAborted
//   Value that receives TRUE if the user aborted any file operations before they
//   were completed or FALSE otherwise.

#ENDIF

// Performs a copy, move, rename, or delete operation on a file system object.
// This can be a file or a folder.
// With thanks to Andrew S Kaplan
External_function vWin32_SHFileOperation "SHFileOperationA" Shell32.dll ;
        Pointer lpFileOp Returns Integer


// Thanks To Oliver Nelson for posting this code on the newsgroups
External_function vWin32_GetWindowsDirectory "GetWindowsDirectoryA" kernel32.dll ;
                           Pointer lpBuffer Integer nSize Returns Integer



// Courtesy Of Vincent Oorsprong
//External_Function vWin32_GetTempFileName "GetTempFileNameA" Kernel32.Dll ;
//   Pointer lpPathName ;
//   Pointer lpPrefixString ;
//   Integer uUnique ;
//   Pointer lpTempFileName ;
//   Returns Integer

External_function vWin32_GetTempFileName "GetTempFileNameA" kernel32.dll String sPath ;
        String sPrefix Integer iUnique Pointer pLoad Returns Integer

External_function vWin32_GetTempPath "GetTempPathA" Kernel32.Dll ;
   Dword nBufferLength ;
   Pointer lpBuffer ;
   Returns Integer

External_function vWin32_DeleteFile "DeleteFileA" Kernel32.Dll ;
   Pointer lpFileName ;
   Returns Integer
                  


Define vCSIDL_DESKTOP          For |CI$00
Define vCSIDL_PROGRAMS         For |CI$02
Define vCSIDL_CONTROLS         For |CI$03
Define vCSIDL_PRINTERS         For |CI$04
Define vCSIDL_PERSONAL         For |CI$05   // (Documents folder)
Define vCSIDL_FAVORITES        For |CI$06
Define vCSIDL_STARTUP          For |CI$07
Define vCSIDL_RECENT           For |CI$08   // (Recent folder)
Define vCSIDL_SENDTO           For |CI$09
Define vCSIDL_BITBUCKET        For |CI$0A
Define vCSIDL_STARTMENU        For |CI$0B
Define vCSIDL_DESKTOPDIRECTORY For |CI$10
Define vCSIDL_DRIVES           For |CI$11
Define vCSIDL_NETWORK          For |CI$12
Define vCSIDL_NETHOOD          For |CI$13
Define vCSIDL_FONTS            For |CI$14
Define vCSIDL_TEMPLATES        For |CI$15   // (ShellNew folder)


//HRESULT SHGetFolderPath(
//    HWND hwndOwner,
//    int nFolder,
//    HANDLE hToken,
//    DWORD dwFlags,
//    LPTSTR pszPath
//);
// This function is a superset of SHGetSpecialFolderPath, included with earlier versions of
// the shell. It is implemented in a redistributable DLL, SHFolder.dll, that also simulates
// many of the new shell folders on older platforms such as Windows 95, Windows 98, and
// Windows NT 4.0. This DLL always calls the current platform's version of this function.
// If that fails, it will try to simulate the appropriate behavior.
//
External_function vWin32_SHGetFolderPath "SHGetFolderPathA" SHFolder.Dll ;
   Pointer hWnd ;
   Integer nFolder ;
   Pointer hToken ;
   DWord   dwFlags ;
   Pointer lpszPath ;
   Returns Integer



Type vWin32_Find_Data
  Field vWin32_Find_Data.dwFileAttributes As Dword
  Field vWin32_Find_Data.ftCreationLowDateTime As Dword
  Field vWin32_Find_Data.ftCreationHighDateTime As Dword
  Field vWin32_Find_Data.ftLastAccessLowDateTime As dword
  Field vWin32_Find_Data.ftLastAccessHighDateTime As Dword
  Field vWin32_Find_Data.ftLastWriteLowDateTime As Dword
  Field vWin32_Find_Data.ftLastWriteHighDateTime As Dword
  Field vWin32_Find_Data.nFileSizeHigh As Dword
  Field vWin32_Find_Data.nFileSizeLow As Dword
  Field vWin32_Find_Data.dwReserved0 As Dword
  Field vWin32_Find_Data.dwReserved1 As Dword
  Field vWin32_Find_Data.cFileName As Char vMax_Path
  Field vWin32_Find_Data.cAlternateFileName As Char 14
End_Type // vWin32_Find_Data

// Courtesy Of Vincent Oorsprong
// lpFileName      : address of name of file to search for
// lpFindFileData  : address of returned information
External_function vWin32_FindFirstFile "FindFirstFileA"  Kernel32.dll Pointer lpFileName ;
          Pointer lpFindFileData Returns Handle

// Courtesy Of Vincent Oorsprong
// hFindFile       : handle of search
// lpFindFileData  : address of structure for data on found file
External_function vWin32_FindNextFile "FindNextFileA" Kernel32.dll Handle hFindFile ;
          Pointer lpFindFileData Returns Integer

// Courtesy Of Vincent Oorsprong
//  hFindFile      : file search handle
External_function vWin32_FindClose "FindClose" Kernel32.dll Handle hFindFile Returns Integer



Type vFileTime
  Field vFileTime.dwLowDateTime As Dword
  Field vFileTime.dwHighDateTime As Dword
End_Type // vFileTime


Type vSystemTime
  Field vSystemTime.wYear As Word
  Field vSystemTime.wMonth As Word
  Field vSystemTime.wDayOfWeek As Word
  Field vSystemTime.wDay As Word
  Field vSystemTime.wHour As Word
  Field vSystemTime.wMinute As Word
  Field vSystemTime.wSecond As Word
  Field vSystemTime.wMilliSeconds As Word
End_Type // vSystemTime


// Courtesy Of Vincent Oorsprong
//  lpFileTime     : pointer to file time to convert
//  lpSystemTime   : pointer to structure to receive system time
External_function vWin32_FileTimeToSystemTime "FileTimeToSystemTime" Kernel32.Dll ;
          Pointer lpFileTime Pointer lpsystemTime Returns Integer

// Courtesy Of Vincent Oorsprong
// This function formats the time in a picture-string passed
//
// Picture      Meaning
//    h         Hours with no leading zero for single-digit hours; 12-hour clock
//    hh        Hours with leading zero for single-digit hours; 12-hour clock
//    H         Hours with no leading zero for single-digit hours; 24-hour clock
//    HH        Hours with leading zero for single-digit hours; 24-hour clock
//    m         Minutes with no leading zero for single-digit minutes
//    mm        Minutes with leading zero for single-digit minutes
//    s         Seconds with no leading zero for single-digit seconds
//    ss        Seconds with leading zero for single-digit seconds
//    t         One character time marker string, such as A or P
//    tt        Multicharacter time marker string, such as AM or PM
//
// For example, to get the time string  "11:29:40 PM"
//    use the following picture string: "hh" : "mm" : "ss tt"

External_function vWin32_GetTimeFormat "GetTimeFormatA" Kernel32.Dll ;
  Dword LCID Dword dwFlags Pointer lpsSystemTime Pointer lpFormat Pointer lpTimeStr ;
  Integer cchTime Returns Integer


// Courtesy Of Vincent Oorsprong
// This function formats the date in a picture-string passed
//
// Picture      Meaning
//    d         Day of month as digits with no leading zero for single-digit days.
//    dd        Day of month as digits with leading zero for single-digit days.
//    ddd       Day of week as a three-letter abbreviation. The function uses the
//              LOCALE_SABBREVOAYMAME value associated with the specified locale.
//    dddd      Day of week as its full name. The function uses the LOCALE_SDAYNAME
//              value associated with the specified locale.
//    M         Month as digits with no leading zero for single-digit months.
//    MM        Month as digits with leading zero for single-digit months.
//    MMM       Month as a three-letter abbreviation. The function uses the
//              LOCALE_SABBREVMONTHNAME value associated with the specified locale.
//    MMMM      Month as its full name. The function uses the LOCALE_SMONTHNAME value
//              associated with the specified locale.
//    y         Year as last two digits, but with no leading zero for years less than 10.
//    yy        Year as last two digits, but with leading zero for years less than 10.
//    yyyy      Year represented hy full four digits.
//    gg        Period/era string. The function uses the CAL_SERASTRING value associated
//              with the specified locale. This element is ignored if the date to be formatted
//              does not have an associated era or period string.
// For example, to get the date string  "Wed, Aug 31 94"
// use the following picture string:    "ddd","MMM dd yy"

External_function vWin32_GetDateFormat "GetDateFormatA" Kernel32.Dll ;
  Dword LCID Dword dwFlags Pointer lpsSystemTime Pointer lpFormat Pointer lpDateStr ;
  Integer cchDate Returns Integer

Define LOCALE_NOUSEROVERRIDE    For |CI$80000000  //  do not use user overrides
Define TIME_NOMIHUTESORSECONDS  For |CI$0000000l  //  do not use minutes or seconds
Define TIME_NOSECONDS           For |CI$00000002  //  do not use seconds
Define TIME_NOTIMEMARKER        For |CI$00000004  //  do not use time marker
Define TIME_FORCE24HOURFORMAT   For |CI$00000008  //  always use 24 hour format

//  Date Flags for GetDateFormatW.
//
Define DATE_SHORTDATE           For |CI$0000000l  //  use short date picture
Define DATE_LONGDATE            For |CI$00000002  //  use long date picture
Define DATE_USE_ALT_CALENDAR    For |CI$00000004  //  use alternate calendar (if any)


External_function vWin32_SetLastError "SetLastError" Kernel32.Dll Dword dwLastError Returns Integer


// **WvA: 20-02-2004
// While i was testing the format capabilities i stumbled over a very
// weird problem where it looks like that the integer value gets somehow translated
// incorrectly into an unsigned integer.
// I don't have the time to dive into this.. so added a workaround (and removed it again)
// It does smell a bit fishy though
// Tested it with the same results on both VDF7 and VDF9.1

Define SHFMT_ID_DEFAULT        For |CI$0000FFFF // The default format ID
Define SHFMT_OPT_DEFAULT       For |CI$00000000 // The default "Quick Format" option.
Define SHFMT_OPT_FULL          For |CI$00000001 // Deselects the "Quick Format" option, providing a full format instead. This is useful when an unformatted disk is detected.
Define SHFMT_OPT_SYSONLY       For |CI$00000002 // Selects the "Create an MS-DOS startup disk" option, creating a system boot disk.

// Possible errors that can be returned by the shellformat function
Define SHFMT_ERROR             For (|CI$FFFFFFFF+1) // An error occurred during the last format or no drive parameter passed. This does not indicate that the disk is unformatable.
Define SHFMT_CANCEL            For (|CI$FFFFFFFE+1) // The last format was canceled.
Define SHFMT_NOFORMAT          For (|CI$FFFFFFFD+1) // The drive cannot be formatted.
                                                

 // Courtesy Of Steve Walter,
 // USA Software, Inc
 // Format a disk
 // Called By:  Move (vWin32_ShFormatDrive(hWnd,0,$FFFF,1)) To dwReturn
External_function vWin32_ShFormatDrive "SHFormatDrive" shell32.dll Handle hWnd ;
         Integer iDrive Integer iFormatID Integer iOptions Returns DWORD
                  